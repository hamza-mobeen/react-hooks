import React, {useState} from 'react';
import Todo from './Components/Todo';
import Header from './Components/Header';
import Auth from './Components/Auth';
import AuthContext from './auth-context';

function App(props) {
  const [page , setPage] = useState('Auth');
  const [authStatus, setAuthStatus]= useState(false);

  const SwitchHandler = (pageName) => {
    setPage(pageName);
  }
  const login = () => {
    setAuthStatus(true);
  };

  return (
    <div className="App">
      <AuthContext.Provider value={{status: authStatus, login: login}}>
      <Header onLoadTodos={SwitchHandler.bind(this, 'todos')} onLoadAuth={SwitchHandler.bind(this, 'auth')}/>
      <hr />
      {page ==='auth' ? <Auth /> : <Todo /> }
     
      </AuthContext.Provider>
    </div>
  );
}

export default App;
