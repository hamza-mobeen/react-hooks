import React from 'react';

const list = props => {
    console.log('Rendering the list');

    return (
        <ul>
            {props.Lists.map(listItem => (
                <li key={listItem.id} onClick={props.onClick.bind(this, listItem.id)}>{listItem.state}</li>
            ))}
        </ul>
    );
}

export default list;