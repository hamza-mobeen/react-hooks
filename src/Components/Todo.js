import React, { useState, useEffect, useReducer, useRef, useMemo } from "react";
import axios from "axios";
import List from "./List";
import { useFormInput } from "../Hooks/Forms";

const Todo = (props) => {
    const [inputIsValid, setInputIsValid] = useState(false);

    // const todoInputRef = useRef();

    const todoInput = useFormInput();


    const todoListReducer = (state, action) => {
        switch (action.type) {
            case 'ADD':
                return state.concat(action.payload);
            case 'SET':
                return action.payload;
            case 'REMOVE':
                return state.filter((todo) => todo.id !== action.payload);
            default:
                return state;
        }
    };
    const [inputList, dispatch] = useReducer(todoListReducer, []);

    const UpdateEventHandler = (event) => {

        if (event.target.value.trim() === '') {
            setInputIsValid(false);
        }
        else {
            setInputIsValid(true);
        }
    }
    useEffect(() => {

        axios.get('https://react-hook-hamza-default-rtdb.firebaseio.com/todos.json')
            .then(
                result => {
                    console.log(result);
                    const inputData = result.data;
                    const Data = [];
                    for (var key in inputData) {
                        Data.push({ id: key, state: inputData[key].State })
                    }
                    dispatch({ type: 'SET', payload: Data });
                });
        return () => {
            console.log('cleanup');
        }
    }, []);

    // useEffect(() => {
    //     if (submitTodo) {
    //         dispatch({ type: 'ADD', payload: submitTodo });
    //     }

    // }, [submitTodo]);

    const todoRemoveHandler = todoId => {
        axios.delete(`https://react-hook-hamza-default-rtdb.firebaseio.com/todos/${todoId}.json`)
            .then(res => {
                dispatch({ type: 'REMOVE', payload: todoId });
            })
            .catch(err => console.log(err));
    }

    // const inputChangeHandler = (event) => {
    //     setInputState(event.target.value)
    // };
    const todoListHandler = () => {
        const inputState = todoInput.value;

        axios.post('https://react-hook-hamza-default-rtdb.firebaseio.com/todos.json', { State: inputState })
            .then(res => {
                setTimeout(() => {
                    const Items = { id: res.data.state, state: inputState };
                    dispatch({ type: 'ADD', payload: Items });
                }, 1000);
            })
            .catch((err) => {
                console.log(err);
            });
    };
    return (
        <React.Fragment>
            <input type="text"
                placeholder="todo"
               onChange={todoInput.onchange}
               value= {todoInput.value}
               
                style={{ backgroundColor: todoInput.validity === true ? "transparent" : "red" }} />
            <button type="button" onClick={todoListHandler}>Add</button>

            {useMemo(() => <List Lists={inputList} onClick={todoRemoveHandler} />, [inputList])}
        </React.Fragment>
    );
}
export default Todo;